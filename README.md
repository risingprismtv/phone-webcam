# Phone Webcam

Commands for setting up your phone as a webcam.

```
sudo modprobe v4l2loopback devices=1 max_buffers=2 exclusive_caps=1 card_label="VirtualCam #0"
v4l2-ctl --list-devices
ffmpeg -i http://192.X.X.X:8080/videofeed -f v4l2 -pix_fmt yuv420p /dev/videoY #Replace X with your real IP! Replace Y with your dummy device from step 2!
```